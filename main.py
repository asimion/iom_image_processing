import sys
import os
import cv2
from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QDialog, QApplication, QFileDialog
from PyQt5.uic import loadUi
from PyQt5 import QtGui
from ImageAlgorithms import ImageAlgorithms as ImageAlgorithms
from MyExceptions import GrayscaleException as GrayscaleException
currentDir = os.getcwd()

class Interface_GUI(QDialog):
    def __init__(self):
        super(Interface_GUI, self).__init__()
        loadUi('interface.ui', self)
        self.imageAlgorithms = None
        self.loadButton.clicked.connect(self.loadClicked)
        self.saveButton.clicked.connect(self.saveClicked)
        self.comboBox.activated[str].connect(self.onActivated)
        # self.comboBox.activated[str].connect(self.cannyClicked)
        # self.comboBox.activated[str].connect(self.cannyClicked)


    @pyqtSlot()
    def onActivated(self):
        img = self.imageAlgorithms.getImage()
        if self.comboBox.currentText() == "Median":
            self.imageAlgorithms.setImage(self.imageAlgorithms.medianFilter(9))
        elif self.comboBox.currentText() == "Laplacian":
            self.imageAlgorithms.setImage(self.imageAlgorithms.laplacianFilter())
        elif self.comboBox.currentText() == "Gaussian":
            self.imageAlgorithms.setImage(self.imageAlgorithms.gaussianFilter(9))
        self.displayImage(2)
        self.imageAlgorithms.setImage(img)

    @pyqtSlot()
    def loadClicked(self):
        fname, _ = QFileDialog.getOpenFileName(
            self, 'Open File', currentDir)
        if fname:
            self.loadImage(fname)
        else:
            print('Invalid Image')

    @pyqtSlot()
    def saveClicked(self):
        fname, _ = QFileDialog.getSaveFileName(
            self, 'Save File', currentDir)
        if fname:
            cv2.imwrite(fname, self.imageAlgorithms.getImage())
        else:
            print('Error')

    def loadImage(self, fname):
        try:
        	self.imageAlgorithms = ImageAlgorithms(fname)
        	self.imageAlgorithms.isGrayscale()
        except GrayscaleException:
            self.imageAlgorithms.setImage( self.imageAlgorithms.convertRGBToGray() )
        self.displayImage(1)

    def displayImage(self, window=1):
        qformat = QImage.Format_Indexed8

        if len(self.imageAlgorithms.getImage().shape) == 3:  # rows[0],cols[1],channels[2]
            if(self.imageAlgorithms.getImage().shape[2]) == 4:
                qformat = QImage.Format_RGBA8888
            else:
                qformat = QImage.Format_RGB888
        img = QImage(
            self.imageAlgorithms.getImage(), self.imageAlgorithms.getImage().shape[1], self.imageAlgorithms.getImage().shape[0], self.imageAlgorithms.getImage().strides[0], qformat)
        #BGR > RGB
        pixmap = QtGui.QPixmap(img)
        pixmap4 = pixmap.scaled(512, 512, QtCore.Qt.KeepAspectRatio)
        img = pixmap4

        if window == 1:
            self.imgLabel.setScaledContents(True)
            self.imgLabel.setPixmap(img)
            self.imgLabel.setAlignment(
                QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        if window == 2:
            self.processedLabel.setScaledContents(True)
            self.processedLabel.setPixmap(img)
            self.processedLabel.setAlignment(
                QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Interface_GUI()
    window.setWindowTitle('Image Processing')
    window.show()
    sys.exit(app.exec_())
