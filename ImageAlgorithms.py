"""
    author : asimion
    e-mail : andrei.simion1707@upb.ro
"""
import cv2
from MyExceptions import GrayscaleException as GrayscaleException
import numpy as np

class ImageAlgorithms:
    #costructor params: self , _path[string]
    # _path [string]  == where the image is
    def __init__(self, _path):
        self.image = cv2.imread( _path , cv2.IMREAD_COLOR )
        
    def setImage(self, _image):
        self.image = _image

    def getImage(self):
        return self.image

    # isGrayscale() function :verify if my image is grayscale:
    # raise GrayscaleException if isnt Grayscale
    # else return  --> all good (Grayscale)
    def isGrayscale(self):
        if len(self.image.shape) > 2:
            raise GrayscaleException
        else:
            return True #all good

    #converterRGBToGray() : RGB -> Gray converter
    #return grayscale image
    def convertRGBToGray(self):
        return cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)

    #MEDIAN FILTER
    #w_size = window size
    def medianFilter(self, w_size):
        new_image = cv2.medianBlur(self.image, w_size)
        return new_image

    #LAPLACIAN FILTER
    def laplacianFilter(self):
        new_image = cv2.Laplacian(self.image, cv2.CV_64F)
        return new_image

    #GAUSSIAN FILTER
    #w_size = window size
    def gaussianFilter(self, w_size):
        new_image = cv2.GaussianBlur(self.image, (w_size, w_size),0)
        return new_image

    #Vintange FILTER
    def vintageFilter(self):
        rows, cols = self.image.shape[:2]
        kernel_x = cv2.getGaussianKernel(cols, 200)
        kernel_y = cv2.getGaussianKernel(rows, 200)
        kernel = kernel_y * kernel_x.T
        filter = 255 * kernel / np.linalg.norm(kernel)
        vintage_im = np.copy(self.image)
        for i in range(3):
            vintage_im[:, :, i] = vintage_im[:, :, i] * filter
        return vintage_im

