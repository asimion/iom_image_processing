"""
    author : asimion
    e-mail : andrei.simion1707@upb.ro
"""

class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class GrayscaleException(Error):
    """ Raised when the image isn't grayscale """
    pass
